# basics
puts "hello, world!"

puts ""

puts "hello,\tworld!\n"

# Concatenation and interpolation
puts "foo" + "bar"

puts 'ant' + 'bat' + 'cat'

first_name = 'Edu'
last_name = 'Finn'

puts first_name + ' ' + last_name
puts "#{first_name} #{last_name}"

# Single-quoted strings
puts '\n'

puts 'Newlines (\n) and tabs (\t) both use the backslash character: \.'
puts "Newlines (\\n) and tabs (\\t) both use the backslash character: \\."

puts 'It\'s not easy being green'

# Exercises
city = 'Uerikon'
canton = 'ZH'
puts "#{city}, #{canton}"

# Printing
puts "hello, world!"
puts("hello, world!")

print "hello, world1"
print "hello, world2\n"
print "hello, world3\n"

# Exercises
puts '=== puts'
puts 'hello', 'world'
puts '=== print'
print 'hello', 'world'
print 'again', "\n"
print "and again\n"
puts '==='

# Attributes, booleans, and control flow
puts 'badger'.length
puts 'badger'.length > 3
puts 'badger'.length > 6
puts 'badger'.length >= 6
puts 'badger'.length < 10
puts 'badger'.length == 6

password = 'foo'
if password.length < 6
  puts 'Password is too short.'
end

password = 'foobar'
if password.length < 6
  puts 'Password is too short.'
else
  puts 'Password is long enough.'
end

password = 'goldilocks'
if password.length < 6
  puts 'Password is too short.'
elsif password.length < 50
  puts 'Password is just right!'
else
  puts 'Password is long enough.'
end

password = 'foo'
puts "Password is too short." if password.length < 6
puts "Password is too short." unless password.length >= 6
puts ''

# Combining and inverting booleans
puts "true && false = #{true && false}"
puts "false && true = #{false && true}"
puts "false && false = #{false && false}"
puts "true && true = #{true && true}"
puts ''

x = 'foo'
y = ''

if x.length == 0 && y.length == 0
  puts 'Both strings are empty!'
else
  puts 'At least one of the strings is nonempty.'
end
puts ''

puts "true || false = #{true || false}"
puts "false || true = #{false || true}"
puts "true || true = #{true || true}"
puts "false || false = #{false || false}"
puts ''

if x.length == 0 || y.length == 0
  puts 'At least one of the strings is empty!'
else
  puts 'Neither of the strings is empty.'
end
puts ''

puts "!true = #{!true}"
puts "!false = #{!false}"

if !(x.length == 0)
  puts "x is not empty."
else
  puts "x is empty."
end

puts "!(x.length == 0) = #{!(x.length == 0)}"

if x.length != 0
  puts 'x is not empty.'
else
  puts 'x is empty.'
end
puts ''

# Bang Bang
puts "!!true = #{!!true}"
puts "!!false = #{!!false}"
puts "!!'foo' = #{!!'foo'}"
puts "!!'' = #{!!''}"
puts "!!0 = #{!!0}"
puts "!!nil = #{!!nil}"
puts ''

# Exercises
x = 'foo'
y = ''
puts "x = '#{x}', y = '#{y}'"
puts "(x && y) = '#{(x && y)}'"
puts "(x || y) = '#{(x || y)}'"
puts "!!(x && y) = #{!!(x && y)}"
puts "!!(x || y) = #{!!(x || y)}"
puts ''

# Methods
puts "'badger'.empty? = #{'badger'.empty?}"
puts "''.empty? = #{''.empty?}"

if x.empty? && y.empty?
  puts "Both strings are empty!"
else
  puts "At least one of the strings is nonempty."
end

puts 'HONEY BADGER'.downcase
puts ''

animal = 'HONEY BADGER'
puts animal.downcase
puts animal

username = first_name.downcase
puts "first_name: #{first_name}"
puts "#{username}@example.com"
puts last_name.upcase
puts ''

puts "'hello'.include? 'lo' = #{'hello'.include? 'lo'}"
puts "'hello'.include? 'ol' = #{'hello'.include? 'ol'}"
puts "'hello'.include? ?h = #{'hello'.include? ?h}"
puts ''

soliloquy = 'To be, or not to be, that is the question:'
puts "soliloquy.include?('To be') = #{soliloquy.include?('To be')}"
puts "soliloquy.include?('question') = #{soliloquy.include?('question')}"
puts "soliloquy.include?('nonexistent') = #{soliloquy.include?('nonexistent')}"
puts "soliloquy.include?('TO BE') = #{soliloquy.include?('TO BE')}"
puts ''

# Exercises
badger = 'hoNeY BaDGer'
puts badger.include? 'badger'
puts badger.downcase.include? 'badger'

puts badger.capitalize
all_caps = 'HONEY BADGER'
puts all_caps.capitalize

puts ''.nil?
puts nil.nil?
puts ''

# String iteration
puts soliloquy
puts soliloquy[0]
puts soliloquy[1]
puts soliloquy[2]
puts ''

for i in 0..4 do
  puts i
end
puts ''

puts soliloquy.length
for i in 0..41 do
  puts soliloquy[i]
end

for i in 0..(soliloquy.length - 1) do
  puts soliloquy[i]
end

# Exercises
for i in 1..(soliloquy.length) do
  puts soliloquy[(soliloquy.length - i)]
end
