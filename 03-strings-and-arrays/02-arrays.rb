# Splitting
print 'ant bat cat'.split(' ')
puts ''

print 'ant,bat,cat'.split(',')
puts ''

print 'ant, bat, cat'.split(', ')
puts ''

print 'antheybatheycat'.split('hey')
puts ''

print 'badger'.split('')
puts ''

print 'ant bat cat'.split
puts ''

print "ant     bat\t\tcat\n    duck".split
puts ''

# Exercises
a = 'A man, a plan, a canal, Panama'.split(', ')
puts a.length
print a
puts ''
print a.reverse
puts ''

# Array access
a = 'badger'.split('')
print a
puts ''
puts a[0]
puts a[1]
puts a[2]

soliloquy = 'To be, or not to be, that is the question:'
a = ['badger', 42, soliloquy.include?('To be')]
print a
puts ''
puts a[2]
puts a[3]
puts a[-2]

puts a[a.length - 1]
puts a[-1]
puts a.last

# Exercises
puts !!a[100]
b = 'honey badger'.split('')
for i in 0..(b.length - 1) do
  puts b[i]
end

# Array slicing
a = [42, 8, 17, 99]
print a.slice(2, 2)
puts ''

print a.slice(1..3)
puts ''

print a[2, 2]
puts ''
print a[1..3]
puts ''

print (1..10).to_a
puts ''
print ('a'..'z').to_a
puts ''

# Exercises
ex = (1..10).to_a
print ex.slice(2..(ex.length - 3))
puts
print ex.slice(2..-3)
puts

puts 'ant bat cat'.slice(4..7)

print ('a'..'z').to_a.slice(0..12)
puts

# More array methods
puts
a = [42, 8, 17, 99]
print a; puts
puts a.include?(42)
puts a.include?('foo')

# Sorting and reversing
print a.sort; puts
print a; puts

puts "a.sort! = #{a.sort!}"
puts "a after = #{a}"

puts "a.reverse = #{a.reverse}"
puts "a = #{a}"

# Pushing and popping
a.push(6)
a.push('foo')
puts "a after push #{a}"
puts a.pop
puts a.pop
puts a.pop
puts "a after pop #{a}"

the_answer_to_life_the_universe_and_everything = a.pop
puts the_answer_to_life_the_universe_and_everything

a << 'badger'
puts "#{a}"
a << 'ant' << 'bat' << 'cat'
puts "#{a}"

# Undoing a split
a = ['ant', 'bat', 'cat', 42]
puts "#{a}"
puts "a.join = #{a.join}"
puts "a.join(', ') = #{a.join(', ')}"
puts "a.join(' -- ') = #{a.join(' -- ')}"

# Exercises
puts a == a.join(' ').split(' ')

a.unshift(1, 2)
puts "#{a}"
puts a.shift
puts a.shift
puts "#{a}"
puts

# Array iteration
for i in 0..a.length do
  puts a[i]
end

a.each do |element|
  puts element
end
puts

a.each(&method(:puts))
puts

# Exercises
a.reverse.each do |el|
  puts el
end
