class Phrase < String
    # Returns content for palindrome testing.
    def processed_content
        self.downcase
    end
  
    # Returns true for a palindrome, false otherwise.
    def palindrome?
        processed_content == processed_content.reverse
    end
end
phrase = Phrase.new('Racecar')
p phrase
p phrase.palindrome?

class String
    # Returns content for palindrome testing.
    def processed_content
        self.downcase
    end
  
    # Returns true for a palindrome, false otherwise.
    def palindrome?
        processed_content == processed_content.reverse
    end
end
napoleonsLament = String.new('Able was I ere I saw Elba')
p napoleonsLament
p napoleonsLament.palindrome?
p 'foobar'.palindrome?
p 'Racecar'.palindrome?
p 'Able was I ere I saw Elba'.palindrome?
puts

# Exercises
class String
    def blank?
        !!self.match(/^\s*$/)
    end
end
p ''.blank?
p "\t \n".blank?
p ' '.blank?
p 'foobar'.blank?

class String

    # Returns true for a palindrome, false otherwise.
    def palindrome?
        processed_content == processed_content.reverse
    end
  
    private
  
        # Returns content for palindrome testing.
        def processed_content
            self.downcase
        end
end
p 'racecar'.palindrome?
# next line causes NoMethodError
p 'racecar'.processed_content
