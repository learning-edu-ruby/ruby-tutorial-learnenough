# Inheritance
s = String.new('foobar')
puts s 
p s.class
p s.class.superclass
p s.class.superclass.superclass
p s.class.superclass.superclass.superclass

p 'honey badger'.class
puts

class Phrase < String
    attr_accessor :content
  
    def initialize(content)
        @content = content
    end
  
    # Returns true for a palindrome, false otherwise.
    def palindrome?
        processed_content = self.downcase
        processed_content == processed_content.reverse
    end
end
phrase = Phrase.new('Racecar')
p phrase.palindrome?
puts

class Phrase2 < String
    # Returns true for a palindrome, false otherwise.
    def palindrome?
        processed_content = self.downcase
        processed_content == processed_content.reverse
    end
end
phrase = Phrase2.new('Racecar')
p phrase.palindrome?
p phrase.empty?
p phrase.length
p phrase.scan(/c\w/)
p phrase.class
p phrase.class.superclass
p phrase.class.superclass.superclass
p phrase.class.superclass.superclass.superclass
puts

# Exercise
class PhraseExercise < String
    # Returns true for a palindrome, false otherwise.
    def palindrome?
        processed_content = downcase
        processed_content == processed_content.reverse
    end
end
phrase = PhraseExercise.new('Racecar')
p phrase
p phrase.palindrome?
