# Defining classes
p String.new("Madam I'm Adam")
p Time.new(1969, 7, 20, 20, 17, 40)

class Phrase
end

phrase = Phrase.new
puts phrase

class Phrase2
  def initialize(content)
    @content = content
  end
end

phrase = Phrase2.new("Madam I'm Adam")
# next line causes NoMethodError
# puts phrase.content

class Phrase3
  attr_accessor :content

  def initialize(content)
    @content = content
  end
end

phrase = Phrase3.new("Madam I'm Adam")
puts phrase.content

phrase.content = 'Able was I, ere I saw Elba'
puts phrase.content

def palindrome?(string)
  processed_content = string.downcase
  processed_content == processed_content.reverse
end

phrase = Phrase3.new('Racecar')
puts phrase.content
p palindrome?(phrase.content)

class Phrase4
  attr_accessor :content

  def initialize(content)
    @content = content
  end

  def palindrome?
    processed_content = self.content.downcase
    processed_content == processed_content.reverse
  end
end

phrase = Phrase4.new('Racecar')
puts phrase.content
p phrase.palindrome?

# Exercise
class Phrase5
  attr_accessor :content

  def initialize(content)
    @content = content
  end

  def palindrome?
    processed_content = self.content.downcase
    processed_content == processed_content.reverse
  end

  def louder
    self.content.upcase
  end
end

phrase = Phrase5.new('yo adrian!')
puts phrase.content
p phrase.louder
