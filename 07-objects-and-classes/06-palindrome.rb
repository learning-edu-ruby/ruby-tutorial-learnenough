# Factoring the palindrome code into a module
module Palindrome

    # Returns true for a palindrome, false otherwise.
    def palindrome?
        processed_content == processed_content.reverse
    end
  
    private
  
        # Returns content for palindrome testing.
        def processed_content
            self.to_s.downcase
        end
end

# Including the Palindrome module
class String
    include Palindrome
end

class Integer
    include Palindrome
end
p 'foobar'.palindrome?
p 'Racecar'.palindrome?
p 123.palindrome?
p 1221.palindrome?
puts

# Exercises
p (1..100).min
p (1..100).max
p (1..100).sum
