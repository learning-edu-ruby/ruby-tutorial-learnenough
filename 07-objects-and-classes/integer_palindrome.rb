require './palindrome'

class Integer
    include Palindrome
end

p 123.palindrome?
p 1221.palindrome?
