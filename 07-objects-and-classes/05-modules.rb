# Modules
class String

    # Returns true for a palindrome, false otherwise.
    def palindrome?
        processed_content == processed_content.reverse
    end
  
    private
  
        # Returns content for palindrome testing.
        def processed_content
            self.downcase
        end
end
p 'foobar'.palindrome?
p 'Racecar'.palindrome?
puts

class Integer

    # Returns true for a palindrome, false otherwise.
    def palindrome?
        processed_content == processed_content.reverse
    end
  
    private
  
        # Returns content for palindrome testing.
        def processed_content
            self.to_s
        end
end
p 123.palindrome?
p 1221.palindrome?
