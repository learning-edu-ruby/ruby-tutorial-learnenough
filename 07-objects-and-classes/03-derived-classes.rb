# Derived classes
class Phrase < String
    # Returns content for palindrome testing.
    def processed_content
        self.downcase
    end

    # Returns true for a palindrome, false otherwise.
    def palindrome?
        processed_content == processed_content.reverse
    end
end

# Defines a translated Phrase.
class TranslatedPhrase < Phrase
    attr_accessor :translation

    def initialize(content, translation)
        super(content)
        @translation = translation
    end
end

phrase = Phrase.new('Racecar')
p phrase
p phrase.palindrome?
frase = TranslatedPhrase.new('recognize', 'reconocer')
p frase
p frase.palindrome?
puts

# overriding parent's method
class TranslatedPhrase2 < Phrase
    attr_accessor :translation

    def initialize(content, translation)
        super(content)
        @translation = translation
    end

    # Processes the translation for palindrome testing.
    def processed_content
        self.translation.downcase
    end
end
frase = TranslatedPhrase2.new('recognize', 'reconocer')
p frase
p frase.translation
p frase.palindrome?
puts

# Exercises
class PhraseExercise < String
    # Processes the string from palindrome testing.
    def processor(string)
        string.downcase
    end

    # Returns content for palindrome testing.
    def processed_content
        processor(self)
    end

    # Returns true for a palindrome, false otherwise.
    def palindrome?
        processed_content == processed_content.reverse
    end
end
class TranslatedPhraseExercise < PhraseExercise
    attr_accessor :translation

    def initialize(content, translation)
        super(content)
        @translation = translation
    end

    # Processes the translation for palindrome testing.
    def processed_content
        processor(translation)
    end
end
phrase = PhraseExercise.new('Racecar')
p phrase
p phrase.palindrome?
frase = TranslatedPhraseExercise.new('recognize', 'reconocer')
p frase
p frase.translation
p frase.palindrome?
