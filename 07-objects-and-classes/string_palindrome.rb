require './palindrome'

class String
    include Palindrome
end

p 'foobar'.palindrome?
p 'Racecar'.palindrome?
