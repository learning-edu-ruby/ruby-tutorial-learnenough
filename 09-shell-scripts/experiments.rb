require 'nokogiri'

html = '<p>lorem<sup class="reference">1</sup></p><p>ipsum</p>'
doc = Nokogiri::HTML(html)
p doc
puts

p doc.css('p')
p doc.css('p').length
p doc.css('p')[0].content
puts

p doc.css('.reference')
p doc.css('.reference').length
puts

doc.css('.reference').each { |reference| reference.remove }
p doc.css('p').map { |paragraph| paragraph.content }
