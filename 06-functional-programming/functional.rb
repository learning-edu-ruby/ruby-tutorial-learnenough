# map function
cantons = ['Zurich', 'Bern', 'Appenzell Innerrhoden', 'Appenzell Ausserrhoden']

# urls: Imperative version
def imperative_urls(cantons)
  urls = []
  cantons.each do |canton|
    urls << canton.downcase.split.join('-')
  end
  urls
end
p imperative_urls(cantons)
puts

p [1, 2, 3, 4].map { |n| n * n }

p ['ALICE', 'BOB', 'CHARLIE'].map { |name| name.downcase }
p ['ALICE', 'BOB', 'CHARLIE'].map(&:downcase)
puts

# urls: Functional version
def functional_urls(cantons)
  cantons.map { |canton| canton.downcase.split.join('-') }
end
puts functional_urls(cantons).inspect
puts

# Returns a URL-friendly version of a string.
def urlify(string)
  string.downcase.split.join('-')
end

def imperative_urls2(cantons)
  urls = []
  cantons.each do |canton|
    urls << urlify(canton)
  end
  urls
end
p imperative_urls2(cantons)

def functional_urls2(cantons)
  cantons.map { |canton| urlify canton }
end
puts functional_urls2(cantons).inspect

def functional_urls3(cantons)
  cantons.map(&method(:urlify))
end
puts functional_urls3(cantons).inspect
puts

# select function
puts '==='
# singles: Imperative version
def imperative_singles(cantons)
  singles = []
  cantons.each do |canton|
    if canton.split.length == 1
      singles << canton
    end
  end
  singles
end
puts imperative_singles(cantons).inspect
puts

p 16 % 2 # even
p 17 % 2 # odd
p 16 % 2 == 0 # even
p 17 % 2 == 0 # odd

p [1, 2, 3, 4, 5, 6, 7, 8].select { |n| n % 2 == 0}
p [1, 2, 3, 4, 5, 6, 7, 8].select { |n| n.even? }
p (1..8).select(&:even?)
puts

# singles: Functional version
def functional_singles(cantons)
  cantons.select { |canton| canton.split.length == 1 }
end
puts functional_singles(cantons).inspect
puts

# Exercises
def include_appenzell(cantons)
  cantons.select { |canton| canton.downcase.include?('appenzell') }
end
puts include_appenzell(cantons)
puts '--'

def regex_appenzell(cantons)
  cantons.select { |canton| canton.split(/\s/).length == 2 }
end
puts regex_appenzell(cantons)
puts

# reduce function
numbers = 1..10

# sum: Imperative solution
def imperative_sum(numbers)
  total = 0
  numbers.each do |n|
    total += n
  end
  total
end
puts imperative_sum(numbers)

numbers.reduce(0) do |total, n|
  total += n
  total
end

puts numbers.reduce(0) { |total, n| total += n }
puts numbers.reduce { |total, n| total += n }

# sum: Functional solution
def functional_sum(numbers)
  numbers.reduce { |total, n| total += n }
end
puts functional_sum(numbers)
puts

# lengths: Imperative version
def imperative_lengths(cantons)
  lengths = {}
  cantons.each do |canton|
    lengths[canton] = canton.length
  end
  lengths
end
puts imperative_lengths(cantons)

# lengths: Functional version
def functional_lengths(cantons)
  cantons.reduce({}) do |lengths, canton|
    lengths[canton] = canton.length
    lengths
  end
end
puts functional_lengths(cantons)

def functional_lengths_inject(cantons)
  cantons.inject({}) do |lengths, canton|
    lengths[canton] = canton.length
    lengths
  end
end
puts functional_lengths_inject(cantons)
puts

# Exercises
puts numbers.reduce { |total, n| total *= n }

def functional_singles_reject(cantons)
  cantons.reject { |canton| canton.split.length != 1 }
end
p functional_singles_reject(cantons)
