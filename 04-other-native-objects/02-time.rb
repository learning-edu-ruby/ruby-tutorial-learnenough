# Time
s = String.new('A man, a plan, a canal-Panama!')
puts s
print s.split(', ')
puts

a = Array.new
a << 3 << 4
print a
puts
a << 'hello, world!'
print a
puts

now = Time.new
puts now

now = Time.now
puts now
puts

puts now.year
puts now.day
puts now.month
puts now.hour
puts

moon_landing = Time.new(1969, 7, 20, 20, 17, 40)
puts moon_landing
puts moon_landing.day

moon_landing = Time.utc(1969, 7, 20, 20, 17, 40)
puts moon_landing

now = Time.now.utc
puts now
puts now - moon_landing

puts moon_landing.wday # weekday
DAYNAMES = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
puts DAYNAMES[moon_landing.wday]
puts DAYNAMES[now.wday]

# Exercises
bday = Time.utc(1968, 6, 5, 4, 0, 0)
puts bday - moon_landing

