# Hashes
user = {} # an empty hash
user['first_name'] = 'Edu'
user['last_name'] = 'Finn'

puts user['first_name']
puts user['last_name']
puts user['nonexistent']

puts user
moonman = {'first_name' => 'Buzz', 'last_name' => 'Aldrin'}
puts moonman

# Symbols
print 'name'.split('')
puts
# next line causes an error, since symbol doesn't have string's methods
# :name.split('')

# following 2 line will cause errors
#:foo-bar
#:2foo
puts :'foo-bar'
puts :'2foo'
puts

user = {:name => 'Edu Finn', :email => 'edu@example.com'}
puts user
puts user[:name]
puts user[:password]

user = {name: 'Edu Finn', email: 'edu@example.com'}
puts user
puts

# Nested hashes
params = {}
puts params
params[:user] = {name: 'Edu Finn', email: 'edu@example.com'}
puts params
puts params[:user][:name]

# hash iteration
flash = {success: 'It worked!', danger: 'It failed.'}
flash.each do |key, value|
  puts "Key #{key.inspect} has value #{value.inspect}"
end
puts

puts (1..5).to_a
puts (1..5).to_a.inspect
puts :name, :name.inspect
puts 'It worked!', 'It worked!'.inspect
p :name # shortcut to print using inspect

# exercises
user = {name: 'Edu Finn', password: 'secret', password_confirmation: 'secret'}
p user
p user[:password] == user[:password_confirmation]
