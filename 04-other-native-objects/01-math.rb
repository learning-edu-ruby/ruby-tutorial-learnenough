# Math
puts 1 + 1
puts 2 - 3
puts 2 * 3
puts 10 / 5

puts 10 / 4
puts 2 / 3

puts 10 / 4.0
puts 2 / 3.0
puts

# More advanced operations
puts Math::PI
puts Math.sqrt(2)
puts Math.cos(2 * Math::PI)
puts

puts Math.log(Math::E)
puts Math.log(10)

puts Math.log10(10)
puts Math.log10(1000000)
puts Math.log10(Math::E)
puts

puts 2**3
puts Math::E**100
puts

# Math to string
tau = 2 * Math::PI
puts tau.to_s

puts 6.283185307179586.to_s

puts '6.283185307179586'.to_f
puts '6'.to_i
puts

# Exercises
puts "1.24e6".to_f
puts "1.24e6".to_f.to_s
