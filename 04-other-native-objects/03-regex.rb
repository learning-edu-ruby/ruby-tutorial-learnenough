# Regular expressions
zip_code = /\d{5}/
puts 'no match'.match(zip_code)
puts 'Beverly Hills 90210'.match(zip_code)

s = 'Beverly Hills 90210'
puts 'Its got a ZIP code!' if s.match(zip_code)

s =  "Beverly Hills 90210 was a '90s TV show set in Los Angeles."
s += " 91125 is another ZIP code in the Los Angeles area."
print s.scan(zip_code)
puts
print s.scan(/[A-Z]{2,}/)
puts

# Splitting on regexes
puts
print 'ant bat cat duck'.split(' ')
puts

print 'ant bat cat duck'.split(/\s+/)
puts
print "ant    bat\tcat\nduck".split(/\s+/)
puts
print "ant    bat\tcat\nduck".split
puts

# Exercises
puts
zip_code = /\d{5}-\d{4}/
puts 'ZIP code 10118-0110 (the Empire State Building)'.match(zip_code)

puts
sonnet = "Let me not to the marriage of true minds
Admit impediments. Love is not love
Which alters when it alteration finds,
Or bends with the remover to remove.
O no, it is an ever-fixed mark
That looks on tempests and is never shaken
It is the star to every wand'ring bark,
Whose worth's unknown, although his height be taken.
Love's not time's fool, though rosy lips and cheeks
Within his bending sickle's compass come:
Love alters not with his brief hours and weeks,
But bears it out even to the edge of doom.
  If this be error and upon me proved,
  I never writ, nor no man ever loved."

puts sonnet.split(/\n/).length
