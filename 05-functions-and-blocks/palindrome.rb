# Returns true for a palindrome, false otherwise.
def palindrome?(string)
  string == string.reverse
end

def palindromeIgnoreCase?(string)
  string.downcase == string.downcase.reverse
end

def palindromeIgnoreCaseNoDup?(string)
  processed_content = string.downcase
  processed_content == processed_content.reverse
end
