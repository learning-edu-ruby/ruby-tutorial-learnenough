# Function definition
require 'date'
# function with explicit return
def day_of_the_week(time)
  return Date::DAYNAMES[time.wday]
end
p day_of_the_week(Time.now)

# function with implicit return
def day_of_the_week_no_return(time)
  Date::DAYNAMES[time.wday]
end
p day_of_the_week_no_return(Time.now)

# exercise
def square(number)
  number * number
end
p square 2
p square 12
puts
