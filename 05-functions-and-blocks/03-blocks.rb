# Blocks
(1..5).each { |i| puts i ** 2 }

# second way to define a block
(1..5).each do |number|
  puts number ** 2
  puts '--'
end
puts

['ant', 'bat', 'cat', 42].each do |element|
  puts element
end
puts

3.times { puts 'Betelgeuse!' }
puts

# Yield
def sandwich
  puts 'top bread'
  yield
  puts 'bottom bread'
end

sandwich do
  puts 'mutton, lettuce, and tomato'
end

def tag(tag_name, text)
  html = "<#{tag_name}>#{text}</#{tag_name}>"
  yield html
end

# Wrap some text in a paragraph tag.
tag('p', 'Lorem ipsum dolor sit amet') do |markup|
  puts markup
end

puts 'exercise 1'
99.downto(1) do |n|
  puts "#{n} bottle#{n > 1 ? 's' : ''} of beer on the wall"
end
puts

puts 'exercise 2'
def bad_sandwich(contents)
  puts "top bread"
  contents
  puts "bottom bread"
end

bad_sandwich(puts "mutton, lettuce, and tomato")
