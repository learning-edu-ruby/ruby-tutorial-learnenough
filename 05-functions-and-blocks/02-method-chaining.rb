# Method chaining
p 'racecar'.split('')

a = [ 17, 42, 8, 99 ]
p a.reverse

p ['r', 'a', 'c', 'e', 'c', 'a', 'r'].join
puts

p 'Racecar'.split('').reverse.join
p 'Racecar'.chars.reverse.join
p 'Racecar'.reverse
p 'Racecar'.downcase.reverse
puts

require './palindrome'
p palindrome?('racecar')
p palindrome?('Racecar')
puts
p palindromeIgnoreCase?('racecar')
p palindromeIgnoreCase?('Racecar')
puts
p palindromeIgnoreCaseNoDup?('racecar')
p palindromeIgnoreCaseNoDup?('Racecar')
p palindromeIgnoreCaseNoDup?('Able was I ere I saw Elba')
puts

# Exercises
def email_parts(email)
  email.downcase.split('@')
end
p email_parts 'username@example.com'
p email_parts 'USERNAME@EXAMPLE.COM'

p palindrome?("🏎️")
p palindrome?("🦊")
